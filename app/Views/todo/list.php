<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
</head>
<body>
    <h3><?= $title ?></h3>
    <?= anchor('todo/create', 'Add new task') ?>
    <table class="table">
        <tr>
            <th>Title</th>
            <th>User</th>
            <th>Description</th>
            <th></th>
        </tr>
        <?php foreach ($todos as $todo): ?>
            <tr>
                <td><?= $todo['title'] ?></td>
                <td> <?= $todo['firstname'] . ' ' . $todo['lastname'] ?></td>
                <td> <?= $todo['description'] ?></td>
                <td><?= anchor('todo/delete/' . $todo['id'], 'delete')?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>
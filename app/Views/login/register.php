<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
</head>
<body>
    <h3><?= $title ?></h3>
    <form action="/login/registration">        
        <div class="col-12">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
        <div class="form-group">
            <label for='user'>Username</label>
            <input class="form-control" name="user" placeholder="Enter username" maxlength="30">
        </div>
        <div class="form-group">
            <label for='first_name'>First name</label>
            <input class="form-control" name="first_name" placeholder="Enter first name" maxlength="30">
        </div>
        <div class="form-group">
            <label for='last_name'>Last name</label>
            <input class="form-control" name="last_name" placeholder="Enter last name" maxlength="30">
        </div>
        <div class="form-group">
            <label for='password'>Password</label>
            <input class="form-control" name="password" type="password" placeholder="Enter password" maxlength="30">
        </div>
        <div class="form-group">
            <label for='confirmpassword'>Password again</label>
            <input class="form-control" name="confirmpassword" type="password" placeholder="Enter password again" maxlength="30">
        </div>
        <button class="btn btn-primary">Submit</button>
    </form>
</body>
</html>
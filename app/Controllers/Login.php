<?php namespace App\Controllers;

use App\Models\LoginModel;

const REGISTER_TITLE = 'Todo - Register';
const LOGIN_TITLE = 'Todo - Login';

class Login extends BaseController {

    // Constructor starts session.
    public function __construct() {
        $session = \Config\Services::session();
        $session->start();
}

    public function index() {
        $data['title'] = LOGIN_TITLE;
        print view('templates/header', $data); // Set view and pass data.
        print view('login/login', $data);
        print view('templates/footer', $data);
    }

    public function register() {
        $data['title'] = REGISTER_TITLE;
        print view('templates/header', $data); 
        print view('login/register', $data);
        print view('templates/footer', $data);
    }
    
    public function registration() {
        $model = new LoginModel();

        if (!$this->validate([
            'user' => 'required|min_length[8]|max_length[30]',
            'password' => 'required|min_length[8]|max_length[30]',
            'confirmpassword' => 'required|min_length[8]|max_length[30]|matches[password]',
        ])) {
            print view('templates/header', ['title' => REGISTER_TITLE]); // Pass title here
            print view('login/register');
            print view('templates/footer');
        } else {
            $model->save([
                'username' => $this->request->getVar('user'),
                'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
                'firstname' => $this->request->getVar('first_name'),
                'lastname' => $this->request->getVar('last_name')
            ]);
            return redirect('login');
        }
    }

    public function check() {
        $model = new LoginModel();

        if (!$this->validate([
            'user' => 'required|min_length[8]|max_length[30]',
            'password' => 'required|min_length[8]|max_length[30]',
        ])) {
            print view('templates/header', ['title' => LOGIN_TITLE]);
            print view('login/login');
            print view('templates/footer');
        } else {
            $user = $model->check(
                // Model that checks if user exists
                $this->request->getVar('user'),
                $this->request->getVar('password')
            );
            if ($user) {
                // If a registered user is found, stores into a session and redirects to task page
                $_SESSION['user'] = $user;
                return redirect('todo');
            } else {
                // If a user is not found, redirects to login page
                return redirect('login');
            }
        }
    }
}